<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>QODR-Donation|Input</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<script class="jsbin" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<body>

	<div class="bg-contact3" style="background-image: url('images/gallery9.jpg');">
		<div class="container-contact3">
			<div class="wrap-contact3">
				<form action="proses.php" method="POST" class="contact3-form validate-form" enctype="multipart/form-data">
					<span class="contact3-form-title">
						QODR Donation
						<hr>
						<img src="images/ex2.png" alt="" 
						style="display: block;
						margin-left: auto;
						margin-right: auto;
						margin-top: 10%;">												
					</span>
				    <div class="wrap-input3 validate-input" data-validate="Inputan Tidak Boleh Kosong">
						<input class="input3" type="text" name="pengeluaran" placeholder="Pengeluaran">
						<span class="focus-input3"></span>
					</div>
					<div class="wrap-input3 validate-input" data-validate="Inputan Tidak Boleh Kosong">
						<input class="input3" type="text" name="pemasukkan" placeholder="Pemasukkan">
						<span class="focus-input3"></span>
					</div>
					<div class="wrap-input3 validate-input" data-validate="Inputan Tidak Boleh Kosong">
						<input class="input3" type="text" name="saldo" placeholder="Saldo">
						<span class="focus-input3"></span>
					</div>
					<div class="wrap-input3 validate-input" data-validate="Inputan Tidak Boleh Kosong">
						<input class="input3" type="text" name="rab" placeholder="Rencana Anggaran">
						<span class="focus-input3"></span>
					</div>
					<div class="wrap-input3" data-validate="Inputan Tidak Boleh Kosong">
						<input class="input3" type="text" name="date" placeholder="mm/dd/yyyy" id="datepicker" autocomplete="off">
						<span class="focus-input3"></span>
					</div>
					<div class="wrap-input3 validate-input" data-validate = "Inputan Tidak Boleh Kosong">
						<textarea class="input3" name="message" placeholder="Keterangan/Kegunaan"></textarea>
						<span class="focus-input3"></span>
					</div>
					<button type="submit" class="contact3-form-btn" value="submit" class="contact3-form-btn">
							Kirim
					</button>
			  </form>
			</div>
		</div>
	</div>
<!--jquery-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="js/main.js"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
       $( function() {
         $( "#datepicker" ).datepicker().datepicker('setDate', 'today');
       } );
    </script>

     <script>
       window.dataLayer = window.dataLayer || [];
       function gtag(){dataLayer.push(arguments);}
       gtag('js', new Date());
       gtag('config', 'UA-23581568-13');
     </script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <?php
        	if (isset($_SESSION['notification'])) {
        ?>
      <script>
     		$( document ).ready(function() {
     			const Toast = Swal.mixin({
                 toast: true,
                 position: 'top-end',
                 showConfirmButton: false,
                 timer: 3000,
                 timerProgressBar: true,
                 onOpen: (toast) => {
                 toast.addEventListener('mouseenter', Swal.stopTimer)
                 toast.addEventListener('mouseleave', Swal.resumeTimer)}})
     
                 Toast.fire({
                 icon: 'success',
     			title: 'Data Saved Successfully',
     		})
     		})
      </script>
     <?php
     		session_unset();
     		session_destroy();
     	}
     ?>
</body>
</html>
