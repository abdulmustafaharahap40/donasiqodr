
(function ($) {
    "use strict";


    /*==================================================================
    [ Focus Contact2 ]*/
    $('.input3').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
            
    /*==================================================================
    [ Validate ]*/
    var pengeluaran = $('.validate-input input[name="pengeluaran"]');
    var pemasukkan = $('.validate-input input[name="pemasukkan"]');
    var saldo = $('.validate-input input[name="saldo"]');
    var rab = $('.validate-input input[name="rab"]');
    var message = $('.validate-input textarea[name="message"]');


    $('.validate-form').on('submit',function(){
        var check = true;

        if($(pengeluaran).val().trim() == ''){
            showValidate(pengeluaran);
            check=false;
        }

        if($(pemasukkan).val().trim() == ''){
            showValidate(pemasukkan);
            check=false;
        }

        if($(saldo).val().trim() == ''){
            showValidate(saldo);
            check=false;
        }

        if($(rab).val().trim() == ''){
            showValidate(rab);
            check=false;
        }

        if($(message).val().trim() == ''){
            showValidate(message);
            check=false;
        }

        return check;
    });


    $('.validate-form .input3').each(function(){
        $(this).focus(function(){
           hideValidate(this);
       });
    });

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    

})(jQuery);