<?php 
include_once("api_qodr.php");
$donasi=json_decode($query);
?>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
<link rel="stylesheet" href="./style.css">

</head>
<body>
<!-- partial:index.partial.html -->
<section>
  <!--for demo wrap-->
  <div class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th>Tanggal</th>
          <th>Pemasukan</th>
          <th>Pengeluaran</th>
          <th>RAB</th>
          <th>KAS</th>
          <th>Keterangan</th>
        </tr>
      </thead>
    </table>
  </div>
  <div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
        <!--foreach-->
        <?php
        foreach ($donasi as $key) {
          ?><tr>
          <td><?=$key->bulan ?></td>
          <td><?=$key->terkumpul ?> </td>
          <td><?=$key->pengeluaran_real ?></td>
          <td><?=$key->rab ?></td>
          <td><?=$key->kas ?></td>
          <td><?=$key->keterangan ?></td>
        </tr>
         <?php  
             }
           ?>        
      </tbody>
    </table>
  </div>
</section>
<!-- partial -->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script><script  src="./script.js"></script>

</body>
</html>
